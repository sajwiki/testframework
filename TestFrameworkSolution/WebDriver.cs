﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;

namespace TestFrameworkSolution
{
    public static class WebDriver
    {
        private static IWebDriver _broweser;

        public static IWebDriver GetBrowser(string browser)
        {
            if(browser == "Firefox")
            {
                _broweser = new FirefoxDriver();
            }

            if (browser == "Chrome")
            {
                _broweser = new ChromeDriver();
            }

            return _broweser;

        }

    }
}
